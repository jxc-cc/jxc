package com.atguigu.jxc.result;

import lombok.Getter;

/**
 * 统一返回结果状态信息类
 *
 */
@Getter
public enum ResultCodeEnum {

    SUCCESS(200,"成功"),
    FAIL(201, "失败"),
    SERVICE_ERROR(2012, "服务异常"),

    PAY_RUN(205, "支付中"),

    SKU_NOT_EXIST(20001,"sku不存在"),

    LOGIN_AUTH(208, "未登陆"),
    PERMISSION(209, "没有权限"),
    SECKILL_NO_START(210, "秒杀还没开始"),
    SECKILL_RUN(211, "正在排队中"),
    SECKILL_NO_PAY_ORDER(212, "您有未支付的订单"),
    SECKILL_FINISH(213, "已售罄"),
    SECKILL_END(214, "秒杀已结束"),
    SECKILL_SUCCESS(215, "抢单成功"),
    SECKILL_FAIL(216, "抢单失败"),
    SECKILL_ILLEGAL(217, "请求不合法"),
    SECKILL_ORDER_SUCCESS(218, "下单成功"),
    COUPON_GET(220, "优惠券已经领取"),
    COUPON_LIMIT_GET(221, "优惠券已发放完毕"),
    INTERNAL_ERROR(500, "炸了"),

    //------------以 30xxx 的所有码都是  用户相关的错误
    USER_PWD_INVAILD(30001,"账号/密码错误"),
    //------------以 40xxx 的所有码都是  购物车相关的错误
    CART_ITEM_LIMIT(40001,"购物车中商品条目超限"),
    CART_TEMPITEM_NEEDCLEAR(40002, "你先退出登录，把你没登录前的购物车中数据清理一些"),
    CART_ITEM_SIZE_LIMIT(40003,"商品数量不能超过100个" ),
    //------------以 50xxx 的所有码都是  订单相关的错误
    ORDER_MULTI_SUBMIT(50001,"订单重复提交，请刷新再试"),
    ORDER_HAS_NOSTOCK_SKU(50002,"订单中部分商品无货。请移除无货商品再下单；无货商品清单："),
    ORDER_HAS_PRICECHANGE_SKU(50003,"订单中部分商品价格变化。请刷新确认；如下商品价格有更新：")
    //-----------
    ;


    private Integer code;

    private String message;

    private ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
