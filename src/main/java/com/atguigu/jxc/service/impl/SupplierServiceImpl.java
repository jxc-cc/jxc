package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    SupplierDao supplierDao;

    @Override
    public Map<String, Object> getSupplier(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();
        page = (page - 1) * rows;
        List<Supplier> suppliers = supplierDao.getSupplier(page, rows, supplierName);
        Integer total = supplierDao.getTotal(supplierName);
        map.put("total", total);
        map.put("rows", suppliers);
        return map;
    }

    @Override
    public void save(Supplier supplier, Long supplierId) {
        if (supplierId == null) {
//            添加
            supplierDao.save(supplier);
        } else {
//            修改
            supplierDao.update(supplier, supplierId);
        }
    }

    @Override
    public void deleteByIds(String ids) {
        String[] split = ids.split(",");
        List<String> list = new ArrayList<>();
        Arrays.stream(split).forEach(item->list.add(item));
        supplierDao.deleteByIds(list);
    }
}
