package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import javassist.util.HotSwapAgent;
import org.apache.ibatis.session.AutoMappingUnknownColumnBehavior;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> getGoods(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        page = (page - 1) * rows;
        List<Integer> list = getSonType(goodsTypeId);
        Integer total = goodsDao.getTotal(codeOrName, goodsTypeId);
        List<Goods> goods = new ArrayList<>();
        for (Integer integer : list) {
            List<Goods> goodsList = goodsDao.getGoods(page, rows, codeOrName, integer);
            for (Goods goods1 : goodsList) {
                goods.add(goods1);
            }
        }
        map.put("total", total);
        map.put("rows", goods);
        return map;
    }

    public List<Integer> getSonType(Integer goodsTypeId) {
        List<Integer> list = goodsDao.getSonType(goodsTypeId);
        if (list.size() >= 1) {
            return list;
        } else {
            list.add(goodsTypeId);
            return list;
        }
    }

    @Override
    public void saveOrUpdate(Goods goods, Integer goodsId) {
        if (goodsId == null) {
//            添加
            goodsDao.save(goods);
        } else {
//            更新
            goodsDao.update(goods, goodsId);
        }
    }

    @Override
    public void deleteGoods(Integer goodsId) {
        goodsDao.deleteGoods(goodsId);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        page = (page - 1) * rows;
        Integer total = goodsDao.getTotalON(nameOrCode);
        List<Goods> goods = goodsDao.getNoInventoryQuantity(page, rows, nameOrCode);
        map.put("total", total);
        map.put("rows", goods);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        page = (page - 1) * rows;
        Integer total = goodsDao.getTotalIN(nameOrCode);
        List<Goods> goods = goodsDao.getHasInventoryQuantity(page, rows, nameOrCode);
        map.put("total", total);
        map.put("rows", goods);
        return map;
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock(goodsId, inventoryQuantity, purchasingPrice);
    }

    @Override
    public boolean deleteStock(Integer goodsId) {
        boolean d = goodsDao.deleteStock(goodsId);
        return d;
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = new HashMap<>();
        List<Goods> goods = goodsDao.listAlarm();
        map.put("rows", goods);
        return map;
    }
}
