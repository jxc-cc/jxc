package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerDao customerDao;

    @Override
    public Map<String, Object> getCustomer(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        page = (page - 1) * rows;
        Integer total = customerDao.getTatol(customerName);
        List<Customer> customers = customerDao.getCustomer(page, rows, customerName);
        map.put("total", total);
        map.put("rows", customers);
        return map;
    }

    @Override
    public void saveOrUpdate(Customer customer, Integer customerId) {
        if (customerId == null) {
            customerDao.save(customer);
        } else {
            customerDao.update(customer, customerId);
        }
    }

    @Override
    public void deleteByIds(String ids) {
        String[] split = ids.split(",");
        List<String> list = new ArrayList<>();
        Arrays.stream(split).forEach(item -> list.add(item));
        customerDao.deleteByIds(list);
    }
}
