package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/12
 */
@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    OverflowListGoodsDao overflowListGoodsDao;

    @Override
    public void saveOverflowList(OverflowList overflowList, String overflowListGoodsStr, Integer userId) {
//       添加list
        overflowListGoodsDao.saveOverflowList(overflowList, userId);
//        添加goods
        Gson gson = new Gson();
        List<OverflowListGoods> list = gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
        }.getType());
        for (OverflowListGoods overflowListGoods : list) {
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            overflowListGoodsDao.saveOverflowListGoods(overflowListGoods);
        }
    }

    @Override
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> list = overflowListGoodsDao.getOverflowList(sTime, eTime);
        map.put("rows", list);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowListGoods> goods = overflowListGoodsDao.getGoodsList(overflowListId);
        map.put("rows",goods);
        return map;
    }
}
