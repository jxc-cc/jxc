package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    UnitDao unitDao;

    @Override
    public Map<String, Object> getUnit() {
        Map<String, Object> map = new HashMap<>();
        List<Unit> unit = unitDao.getUnit();
        map.put("rows", unit);
        return map;
    }
}
