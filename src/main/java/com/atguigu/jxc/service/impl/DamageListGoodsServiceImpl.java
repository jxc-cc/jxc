package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * title:
 * author:97256
 * date:2023/9/12
 */
@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    DamageListGoodsDao damageListGoodsDao;

    @Override
    public void saveDamageListGoods(DamageList damageList, String damageListGoodsStr, Integer userId) {
//        添加list
        damageListGoodsDao.saveDamageList(damageList, userId);
        Gson gson = new Gson();
        List<DamageListGoods> list = gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {
        }.getType());
        for (DamageListGoods damageListGoods : list) {
            damageListGoods.setDamageListId(damageList.getDamageListId());
            damageListGoodsDao.saveDamageListGoodsStr(damageListGoods);
        }
        System.out.println("1");
    }

    @Override
    public Map<String, Object> getDamageList(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> damageLists = damageListGoodsDao.getDamageList(sTime, eTime);
        map.put("rows", damageLists);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer damageListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageListGoods> list = damageListGoodsDao.getGoodsList(damageListId);
        map.put("rows", list);
        return map;
    }
}
