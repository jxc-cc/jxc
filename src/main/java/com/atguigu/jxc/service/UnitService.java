package com.atguigu.jxc.service;

import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
public interface UnitService {
    Map<String, Object> getUnit();
}
