package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
public interface CustomerService {
    Map<String, Object> getCustomer(Integer page, Integer rows, String customerName);

    void saveOrUpdate(Customer customer, Integer customerId);

    void deleteByIds(String ids);
}
