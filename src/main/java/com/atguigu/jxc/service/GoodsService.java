package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    Map<String, Object> getGoods(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    void saveOrUpdate(Goods goods, Integer goodsId);

    void deleteGoods(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    boolean deleteStock(Integer goodsId);

    Map<String, Object> listAlarm();

}
