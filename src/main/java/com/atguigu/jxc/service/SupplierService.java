package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
public interface SupplierService {
    Map<String, Object> getSupplier(Integer page, Integer rows, String supplierName);

    void save(Supplier supplier,Long supplierId);


    void deleteByIds(String ids);
}
