package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/12
 */
public interface DamageListGoodsService {

    void saveDamageListGoods(DamageList damageList, String damageListGoodsStr,Integer userId);

    Map<String, Object> getDamageList(String sTime, String eTime);

    Map<String, Object> getGoodsList(Integer damageListId);
}
