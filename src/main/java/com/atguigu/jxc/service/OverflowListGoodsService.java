package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/12
 */
public interface OverflowListGoodsService {
    void saveOverflowList(OverflowList overflowList, String overflowListGoodsStr,Integer userId);

    Map<String, Object> getOverflowList(String sTime, String eTime);

    Map<String, Object> getGoodsList(Integer overflowListId);


}
