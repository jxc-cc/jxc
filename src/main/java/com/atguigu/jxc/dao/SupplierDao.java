package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */

public interface SupplierDao {
    List<Supplier> getSupplier(@Param("page") Integer page, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    Integer getTotal(@Param("supplierName") String supplierName);

    void save(@Param("supplier") Supplier supplier);

    void update(@Param("supplier") Supplier supplier, @Param("supplierId") Long supplierId);


    void deleteByIds(@Param("list") List<String> list);
}
