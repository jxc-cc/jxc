package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
public interface CustomerDao {
    List<Customer> getCustomer(@Param("page") Integer page, @Param("rows") Integer rows, @Param("customerName") String customerName);

    void save(@Param("customer") Customer customer);

    void update(@Param("customer") Customer customer, @Param("customerId") Integer customerId);

    void deleteByIds(@Param("list") List<String> list);

    Integer getTatol(@Param("customerName") String customerName);
}
