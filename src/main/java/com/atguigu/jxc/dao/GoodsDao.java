package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<Goods> getGoods(@Param("page") Integer page, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer getTotal(String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    void save(@Param("goods") Goods goods);

    void update(@Param("goods") Goods goods, @Param("goodsId") Integer goodsId);

    void deleteGoods(Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("page") Integer page, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("page") Integer page, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);


    Integer getTotalIN(String nameOrCode);

    Integer getTotalON(String nameOrCode);

    void saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    boolean deleteStock(@Param("goodsId") Integer goodsId);

    List<Goods> listAlarm();

    List<Integer> getSonType(@Param("goodsTypeId") Integer goodsTypeId);
}
