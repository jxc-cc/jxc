package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void save(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId, @Param("state") Integer state);

    void deleteType(@Param("goodsTypeId") Integer goodsTypeId);

    void upParentState(@Param("pId") Integer pId,@Param("state") Integer state);

    Integer getPid(@Param("goodsTypeId") Integer goodsTypeId);

    List<Integer> getGoodsType(@Param("pid") Integer pid);
}
