package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
public interface UnitDao {
    List<Unit> getUnit();
}
