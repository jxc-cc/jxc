package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * title:
 * author:97256
 * date:2023/9/12
 */
public interface DamageListGoodsDao {
    void saveDamageList(@Param("damageList") DamageList damageList,@Param("userId") Integer userId);

    void saveDamageListGoodsStr(@Param("goods") DamageListGoods goods);

    List<DamageList> getDamageList(@Param("sTime") String sTime,@Param("eTime") String eTime);

    List<DamageListGoods> getGoodsList(@Param("damageListId") Integer damageListId);
}
