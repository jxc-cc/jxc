package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * title:
 * author:97256
 * date:2023/9/12
 */
public interface OverflowListGoodsDao {
    void saveOverflowList(@Param("overflowList") OverflowList overflowList,@Param("userId") Integer userId);

    void saveOverflowListGoods(@Param("goods") OverflowListGoods overflowListGoods);

    List<OverflowList> getOverflowList(@Param("sTime") String sTime,@Param("eTime") String eTime);

    List<OverflowListGoods> getGoodsList(@Param("overflowListId") Integer overflowListId);
}
