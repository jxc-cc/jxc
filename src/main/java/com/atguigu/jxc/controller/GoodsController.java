package com.atguigu.jxc.controller;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.result.Result;
import com.atguigu.jxc.service.GoodsService;
import com.google.gson.Gson;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.rmi.MarshalledObject;
import java.util.List;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RequestMapping("/goods")
@RestController
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/listInventory")
    public Map<String, Object> getGoods(@RequestParam(value = "page", required = false) Integer page,
                                        @RequestParam("rows") Integer rows,
                                        @RequestParam(value = "codeOrName", required = false) String codeOrName,
                                        @RequestParam(value = "goodsTypeId", required = false) Integer goodsTypeId) {
        Map<String, Object> goods = goodsService.getGoods(page, rows, codeOrName, goodsTypeId);
        return goods;
    }


    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> getGoodsList(@RequestParam(value = "page", required = false) Integer page,
                                            @RequestParam("rows") Integer rows,
                                            @RequestParam(value = "goodsName", required = false) String goodsName,
                                            @RequestParam(value = "goodsTypeId", required = false) Integer goodsTypeId) {
        Map<String, Object> goods = goodsService.getGoods(page, rows, goodsName, goodsTypeId);
        return goods;
    }


    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(Goods goods,
                                  @RequestParam(value = "goodsId", required = false) Integer goodsId) {
        goodsService.saveOrUpdate(goods, goodsId);
        return new ServiceVO(100, "请求成功", null);
    }

    /**
     * 删除商品信息
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam Integer goodsId) {
        goodsService.deleteGoods(goodsId);
        return new ServiceVO(100, "请求成功", null);
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String, Object> getNoInventoryQuantity(@RequestParam(value = "page", required = false) Integer page,
                                                      @RequestParam("rows") Integer rows,
                                                      @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {
        Map<String, Object> map = goodsService.getNoInventoryQuantity(page, rows, nameOrCode);
        return map;
    }


    /**
     * 分页查询有库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getHasInventoryQuantity")
    public Map<String, Object> getHasInventoryQuantity(@RequestParam(value = "page", required = false) Integer page,
                                                       @RequestParam("rows") Integer rows,
                                                       @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {
        Map<String, Object> map = goodsService.getHasInventoryQuantity(page, rows, nameOrCode);
        return map;
    }


    /**
     * 添加商品期初库存
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     * @return
     */
    @PostMapping("/saveStock")
    public ServiceVO saveStock(@RequestParam(value = "goodsId", required = false) Integer goodsId,
                               @RequestParam(value = "inventoryQuantity", required = false) Integer inventoryQuantity,
                               @RequestParam(value = "purchasingPrice", required = false) double purchasingPrice) {

        goodsService.saveStock(goodsId, inventoryQuantity, purchasingPrice);
        return new ServiceVO(100, "请求成功", null);
    }

    /**
     * 删除商品库存
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(Integer goodsId) {
        boolean b = goodsService.deleteStock(goodsId);
        if (b) {
            return new ServiceVO(100, "请求成功", null);
        }
        return new ServiceVO(1, "不符合条件", null);
    }

    /**
     * 查询库存报警商品信息
     *
     * @return
     */
    @PostMapping("/listAlarm")
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = goodsService.listAlarm();
        return map;
    }

}
