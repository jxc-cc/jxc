package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
@RestController
@RequestMapping("/unit")
public class UnitController {

    @Autowired
    UnitService unitService;

    @PostMapping("/list")
    public Map<String, Object> getUnit() {
        Map<String, Object> map = unitService.getUnit();
        return map;
    }
}
