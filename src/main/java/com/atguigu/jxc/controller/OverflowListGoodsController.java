package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/12
 */
@RequestMapping("/overflowListGoods")
@RestController
public class OverflowListGoodsController {
    @Autowired
    OverflowListGoodsService overflowListGoodsService;

    @PostMapping("/save")
    public ServiceVO saveOverflowList(OverflowList overflowList,
                                      String overflowListGoodsStr, HttpSession session) {

        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        overflowListGoodsService.saveOverflowList(overflowList, overflowListGoodsStr, userId);
        return new ServiceVO(100, "请求成功", null);
    }

    @PostMapping("/list")
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        Map<String, Object> map = overflowListGoodsService.getOverflowList(sTime, eTime);
        return map;
    }

    @PostMapping("/goodsList")
    public Map<String, Object> getGoodsList(Integer overflowListId) {
        Map<String, Object> map = overflowListGoodsService.getGoodsList(overflowListId);
        return map;
    }

}
