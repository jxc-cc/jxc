package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @PostMapping("/list")
    public Map<String, Object> getCustomer(@RequestParam(value = "page", required = false) Integer page,
                                           @RequestParam("rows") Integer rows,
                                           @RequestParam(value = "customerName", required = false) String customerName) {
        Map<String, Object> map = customerService.getCustomer(page, rows, customerName);
        return map;
    }

    @PostMapping("/save")
    public ServiceVO saveOrUpdate(Customer customer,
                                  @RequestParam(value = "customerId", required = false) Integer customerId) {
        customerService.saveOrUpdate(customer, customerId);
        return new ServiceVO(100, "请求成功", null);
    }

    @PostMapping("/delete")
    public ServiceVO deleteByIds(@RequestParam("ids") String ids) {
        customerService.deleteByIds(ids);
        return new ServiceVO(100, "请求成功", null);
    }
}
