package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/11
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    SupplierService supplierService;

    @PostMapping("/list")
    public Map<String, Object> getSupplier(@RequestParam("page") Integer page,
                                           @RequestParam("rows") Integer rows,
                                           @RequestParam(value = "supplierName", required = false) String supplierName) {
        Map<String, Object> map = supplierService.getSupplier(page, rows, supplierName);
        return map;
    }

    @PostMapping("/save")
    public ServiceVO save(Supplier supplier,
                          @RequestParam(value = "supplierId", required = false) Long supplierId) {
        supplierService.save(supplier, supplierId);
        return new ServiceVO(100, "请求成功", null);
    }

    @PostMapping("/delete")
    public ServiceVO deleteById(@RequestParam(value = "ids", required = false) String ids) {
      supplierService.deleteByIds(ids);

        return new ServiceVO(100,"请求成功",null);
    }
}
