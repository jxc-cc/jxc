package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * title:
 * author:97256
 * date:2023/9/12
 */
@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    DamageListGoodsService damageListGoodsService;

    @PostMapping("/save")
    public ServiceVO saveDamageListGoods(DamageList damageList,
                                         String damageListGoodsStr,
                                         HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        damageListGoodsService.saveDamageListGoods(damageList, damageListGoodsStr, userId);
        return new ServiceVO(100, "请求成功", null);
    }

    @PostMapping("/list")
    public Map<String, Object> getDamageList(String sTime,
                                             String eTime) {
        Map<String, Object> map = damageListGoodsService.getDamageList(sTime, eTime);
        return map;
    }

    @PostMapping("/goodsList")
    public Map<String, Object> getGoodsList(Integer damageListId) {
        Map<String, Object> map = damageListGoodsService.getGoodsList(damageListId);
        return map;
    }
}
